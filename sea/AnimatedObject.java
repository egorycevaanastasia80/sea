package sea;

import java.awt.image.BufferedImage;


public class AnimatedObject extends SeaObject {
    //0-вниз, 1-влево, 2-вправо, 3-вверх
    private int direction = 0;   //Направление взгяда
    private int animNumber = 0;  //Номер анимаци

    public AnimatedObject(BufferedImage image, int posX, int posY)  {
       super(image, posX, posY);
    }
     
    public BufferedImage getImage()  {
       BufferedImage image = super.getImage();
       return image.getSubimage(32 * animNumber, 
                                48 * direction, 
                                32, 48);
    }

  public void move(int deltaX, int deltaY)  {
        super.move(deltaX, deltaY);
        //Выбор номера кадра анимации
        animNumber++;
        if (animNumber == 4)  {
            animNumber = 0;
        }
        //Определение направление взгляда
        if (Math.abs(deltaX) > Math.abs(deltaY)) {
            if (deltaX < 0)  {
            direction = 1;
        } else {
            direction = 2;
        }

        } else {
            if (deltaY < 0) {
                direction = 3;
        } else {
             direction = 0;
            } 
         }   
    } 
}
