package sea;

import javax.swing.JPanel;
import java.util.ArrayList;
import java.awt.image.BufferedImage;
import java.awt.Graphics;
import java.awt.Font;

 public class World extends JPanel {
  
    private  ArrayList<SeaObject> seaObjects;
    private BufferedImage background;
    public static int coins = 0;

    
    public World(BufferedImage background, ArrayList<SeaObject> seaObjects) {
    this.background = background;
    this.seaObjects = seaObjects;
    }
    public void paintComponent(Graphics g) {
	    super.paintComponent(g);
      
        int imgSize = 32;
        for (int y = 0; y < this.getHeight(); y += imgSize)  {
            for (int x = 0; x < this.getWidth(); x += imgSize)  {
                g.drawImage(this.background, x, y, null);
            }
         }
           for (SeaObject o : seaObjects) {
               g.drawImage(o.getImage(), 
                           o.getPosX(), 
                           o.getPosY(), 
                           null);
           }          

           g.setFont(new Font("TimesNew", Font.PLAIN, 35));
           g.drawString("Count: " + this.coins, 15, 30);
              this.repaint();
            
              
            }
        
        }
    
        