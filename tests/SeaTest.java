package tests;
import org.junit.Test;
import static org.junit.Assert.*;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.awt.*;
import java.awt.image.*;

import sea.*;

import static org.junit.Assert.assertEquals;

public class SeaTest {

    @org.junit.Test;
    void testMove(){
	File f = new File("image/pirategirl.png");
	BufferedImage playerImage = ImageIO.read(f); 
	Point position = new Point(0, 0);
	SeaObject obj = new SeaObject(playerImage, position.x, position.y);
	assertEquals(position.x, obj.getPosX());
        assertEquals(position.y, obj.getPosY());
	
    }
}
